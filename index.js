const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

app.post('/index', (req, res) => {
    const io = socketIo(server);

    io.on("connect", (socket) => {
        socket.emit("notified", {
            message : "Connected to server!"
        })
    res.send(req.body)
    })

    io.on("notified", (socket) => {
        socket.emit("confirmed", "This one is done")
    })
})

const server = http.Server(app);
server.listen(port);



