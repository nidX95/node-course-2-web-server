const express = require("express");
const bodyParser = require('body-parser');
const routes = require('./routes')
const {client} = require('../lib/redis');

const app = express();
const PORT = process.env.PORT || 8000;

const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(bodyParser.urlencoded())
app.use(bodyParser.json());

http.listen(PORT, () => {
    console.log("Server up on http://localhost:8000")
})

client().then(res => {
    app.get('/list', routes.listAwards)
    // app.post('/add', routes.addAwardEvent)
    io.on('connection', (socket) => {
        console.log('Client connected')
        socket.on('disconnect', () =>{
            console.log('Client disconnected')
        })
        app.post('/add', (req, res) => {
            
            io.emit(req.body.userid, req.body)
            // socket.join(req.body.userid)            
            console.log(`User ${req.body.userid} requesting reward.`)
            if(req.body.userid == undefined || req.body.symbol == undefined || req.body.amount == undefined){
                res.send({
                    message : "Wrong information specified.",
                    status : 400
                })
            }
            else if(io.eventNames().includes("disconnected")){
                res.send({
                    message : "User doesn't exist",
                    status : 404
                })
            }
            else{
                res.send({
                    message : `Awarding user ${req.body.userid} with ${req.body.amount} ${req.body.symbol}`,
                    status : 200
                })
            }
            res.end();
        })
    })
})

