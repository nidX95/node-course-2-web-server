const redis = require('../lib/redis');

const addAward = async (award, cb) => {
    try {
        let client = await redis.client();
        return await client.LPUSH(['awards', JSON.stringify(award)], (err, res) => {
            return cb(res || err) // Number of existing items.
        })
    } catch (error) {
        console.log('Failed to add item')
    }
}

const removeAward = async (cb) => {
    try {
        let client = await redis.client();
        return await client.RPOP('awards', (err, result) =>{
            return cb(result || err);
        });
    } catch (error) {
        console.log(error);
    }
} 

const listAwards = async (cb) => {
    try{
        let client = await redis.client();
        return await client.LRANGE('awards', 0, -1, (err, res) =>{
            cb(res)
        })
    }
    catch(error){
        console.log('Failed to fetch elements.')
    }
}

const quitClient = async (client) => {
    try {
        await client.quit((res) => {
            console.log(res)
        })
    } catch (error) {
        console.log('Error while exiting...')
    }
}

module.exports = {
    addAward,
    removeAward,
    quitClient,
    listAwards
}


