const router = require('express').Router();
const helper = require('../lib/functions');


module.exports.listAwards = router.get('/list', (req, res) => {

   helper.listAwards((result) =>{
        res.send(result)
        res.end()
    })
})

module.exports.addAwardEvent = router.post('/add', (req, res) => {
    if(req.body.userid == undefined || req.body.symbol == undefined || req.body.amount == undefined){
        res.send({
            message : "Wrong information specified.",
            status : 400
        })
    }
    else{
        console.log('Awarding with ', req.body)
        // helper.addAward(req.body, (res) => {
        //     console.log(res) // TODO : SEND NOTIFICATION TO THE APP AND POP REDIS ELEMENT.
        // })
        res.send({
            message : `Awarding user ${req.body.userid} with ${req.body.amount} ${req.body.symbol}`,
            status : 200
        })
    }
    res.end()
}, (awarding) => {
    console.log('awardsiiii')
})