const express = require('express')
const nodemailer = require('nodemailer')
const bodyParser  = require('body-parser')
const {pwd} = require('./hidden')

const app = express()

app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

app.post('/send', (req, res) => {
    let information = JSON.stringify(req.body)

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'stopanca@gmail.com',
          pass: pwd
        },
      });
      let mailOptions = {
        from: 'stopanca@gmail.com',
        to: 'nikola.malic95@gmail.com',
        subject: 'Tokens awarded!',
        text: `${information}`
      };
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      })
      res.send('Confirmed.')
      res.end()

}).listen(process.env.PORT || 8000, console.log(`Listening on port: ${process.env.PORT || 8000}`))
