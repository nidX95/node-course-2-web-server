// Configure Redis and check connection.

const redis = require('redis');
const promise = require('bluebird');

const redisURL = 'redis://192.168.100.179:6379';

promise.promisifyAll(redis.RedisClient.prototype);
promise.promisifyAll(redis.Multi.prototype);

module.exports.client = async () => {
    try {
        let client = await redis.createClient(redisURL);
        client.AUTH('flashboys');
        return client;
    } catch (error) {
        return 500;
    }
}




















